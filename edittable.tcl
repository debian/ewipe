#                                                 #
#                  edittable.tcl                  #       
#                                                 #
#   Copyright (C) 1997-2000  Hiromasa Sekishita   #
#                                                 #
#              This program conforms              #
#      GNU GENERAL PUBLIC LICENSE Version 2.      # 
#                                                 #

proc ET_inittable {text} {
    global et_col
    global et_row
    global et_table

    set et_table {}
    if {$text != {}} {
	set n [llength $text]
	set et_col [llength [lindex $text 0]]
	
	lappend et_table [lindex $text 0]
	set et_row 1
	for {set i 1} {$i < $n} {incr i} {
	    set str [lindex $text $i]
	    if {$str != "|" && $str != "_"} {
		incr et_row
		set tcol {}
		for {set j 0} {$j < [expr $et_col * 2]} {incr j 2} {
		    lappend tcol [lindex $str [expr $j + 1]]
		}
		lappend et_table $tcol
	    }
	}
	if {$et_row == 1} {set et_row 2}
    } else {
	set et_col 3
	set et_row 3
	set et_table {}
    }
    for {set j 0} {$j < 12} {incr j} {
	for {set i 0} {$i < 11} {incr i} {
	    if {[winfo exists .table.c.e($i,$j)] == 1} {
		delete .table.c.e($i,$j)
	    }
	}
    }
    ET_maketable
}

proc ET_settable {} {
    global et_col
    global et_row
    global et_text
    
    set text {}
    append text "\n    "
    set def {}
    for {set i 0} {$i < $et_col} {incr i} {
	set s [.table.c.e($i,0) get]
	if {$s != "l" && $s != "c" && $s != "r"} {
	    set s c
	}
	lappend def $s
    }
    lappend text $def
    append text "\n    "
    lappend text "|"

    append text "\n    "
    for {set j 1} {$j < $et_row} {incr j} {
	set tablec {} 
	for {set i 0} {$i < $et_col} {incr i} {
	    lappend tablec "|"
	    lappend tablec [.table.c.e($i,$j) get]
	}
	lappend tablec "|"
	lappend text $tablec
	append text "\n    "
	lappend text "|"
	append text "\n    "
    }
    destroy .table
    set et_text $text
}

proc ET_maketable {} {
    global et_table
    global et_col
    global et_row

    set width [expr (600 - (5 * $et_col)) / $et_col]
    
    .table.c delete all
 
    for {set i 0} {$i < $et_col} {incr i} {
	if {[winfo exists .table.c.e($i,0)] == 1} {
	    .table.c.e($i,0) configure -width [expr $width / 7]
	} else {
	    entry .table.c.e($i,0) -width [expr $width / 7]
	    set str [lindex [lindex $et_table 0] $i] 
	    if {$str == {}} {set str c}
	    .table.c.e($i,0) insert 0 $str
	}
	set window [.table.c create window [expr $i * ($width + 5)] \
		10 -window .table.c.e($i,0) -anchor nw]
    }

    for {set j 1} {$j < $et_row} {incr j} {
	for {set i 0} {$i < $et_col} {incr i} {
	    if {[winfo exists .table.c.e($i,$j)] == 1} {
		.table.c.e($i,$j) configure -width [expr $width / 7]
	    } else {
		entry .table.c.e($i,$j) -width [expr $width / 7]
		set str [lindex [lindex $et_table $j] $i] 
 		.table.c.e($i,$j) insert 0 $str
	    }
	    set window [.table.c create window [expr $i * ($width + 5)] \
		    [expr $j * 25 + 20] -window .table.c.e($i,$j) -anchor nw]
	}
    }
}

proc edittable {text} {
    global et_col
    global et_row
    global et_text
    global message
    global Option
    
    toplevel .table
    wm title .table "table Editor"
    
    set et_text {}

    frame .table.fb1
    button .table.fb1.bo -text $message(ok) -command ET_settable
    button .table.fb1.bc -text $message(cancel) -command {destroy .table}

    if {$Option(lang) == "jpn"} {
	set btn(addrow) "���ɲ�"
	set btn(delrow) "�Ժ��"
	set btn(addcol) "���ɲ�"
	set btn(delcol) "����"
    } elseif {$Option(lang) == "eng"} {
 	set btn(addrow) "addrow"
	set btn(delrow) "delrow"
	set btn(addcol) "addcolumn"
	set btn(delcol) "delcolumn"
    }
    
    frame .table.fb2
    button .table.fb2.bar -text $btn(addrow) -command {
	incr et_row
	if {$et_row > 11} {set et_row 11}
	ET_maketable
    }
    button .table.fb2.bdr -text $btn(delrow) -command {
	incr et_row -1
	if {$et_row < 2} {set et_row 2}
	ET_maketable
    }
    button .table.fb2.bac -text $btn(addcol) -command {
	incr et_col
	if {$et_col > 10} {set et_col 10}
	ET_maketable
    }
    button .table.fb2.bdc -text $btn(delcol) -command {
	incr et_col -1
	if {$et_col < 1} {set et_col 1}
	ET_maketable
    }
    canvas .table.c -height 310 -width 600
    pack .table.fb2.bar .table.fb2.bdr .table.fb2.bac .table.fb2.bdc \
	    -side left
    pack .table.fb1.bo .table.fb1.bc -side left
    pack .table.c .table.fb2 .table.fb1

    wm minsize .table 600 370
    wm maxsize .table 600 400
    
    ET_inittable $text
    
    update
    focus .table.c    
    grab .table
    tkwait window .table
    
    return $et_text
}
