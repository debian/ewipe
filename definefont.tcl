#                                                 #
#                 definefont.tcl                  #
#                                                 #
#   Copyright (C) 1997-2000  Hiromasa Sekishita   #
#                                                 #
#              This program conforms              #
#      GNU GENERAL PUBLIC LICENSE Version 2.      # 
#                                                 #




# definitions of fonts
# �ե�������

proc definefont {} {
    global deffonts
    global evfont
    global tcl_platform
    global Option

    # set deffonts {14 16 24}
    set deffonts {14 16 18 20 24 26}
    
    set evfont(C,default,14) a14
    # set evfont(C,default,14) rk14
    set evfont(C,default,16) a16
    # set evfont(C,default,16) rk16
    set evfont(C,default,18) a18
    set evfont(C,default,20) a20
    set evfont(C,default,24) a24
    # set evfont(C,default,24) rk24
    set evfont(C,default,26) a26
    
    set evfont(ja,default,14) k14
    set evfont(ja,default,16) k16
    # set evfont(ja,default,16) kanji16
    set evfont(ja,default,18) k18
    set evfont(ja,default,20) k20
    set evfont(ja,default,24) k24
    # set evfont(ja,default,24) kanji24
    set evfont(ja,default,26) k26
    
    if {$tcl_platform(platform) == "windows"} {
	if {$Option(LANG) == "eng"} {
	    set evfont(C,default,14) {{�ͣ� ��ī} 14 normal}
	    set evfont(C,default,16) {{�ͣ� ��ī} 16 normal}
	    set evfont(C,default,18) {{�ͣ� ��ī} 18 normal}
	    set evfont(C,default,20) {{�ͣ� ��ī} 20 normal}
	    set evfont(C,default,24) {{�ͣ� ��ī} 24 normal}
	    set evfont(C,default,26) {{�ͣ� ��ī} 26 normal}
	    set evfont(ja,default,14) {{�ͣ� ��ī} 14 normal}
	    set evfont(ja,default,16) {{�ͣ� ��ī} 16 normal}
	    set evfont(ja,default,18) {{�ͣ� ��ī} 18 normal}
	    set evfont(ja,default,20) {{�ͣ� ��ī} 20 normal}
	    set evfont(ja,default,24) {{�ͣ� ��ī} 24 normal}
	    set evfont(ja,default,26) {{�ͣ� ��ī} 26 normal}
	}
    }


    ##### for tcl/tk 8.0 #####

    if {$tcl_platform(platform) != "windows"} {
	for {set i 0 } {$i < [llength $deffonts]} { incr i } {
	    set size [lindex $deffonts $i]
	    catch {
		font create @evfont(default,$size) \
		    -compound "$evfont(C,default,$size) 
                    $evfont(ja,default,$size)"
		set evfont(C,default,$size) @evfont(default,$size)
	    }
	}
    }
}


